import hashlib
from time import sleep
from etl.model.datagenerator.Basic import Basic

__author__ = 'christian'
import ujson
import os
import sys


# Path for spark source folder
os.environ['SPARK_HOME']="/home/karisu/spark/spark-1.4.0"
# Append pyspark  to Python Path

#sys.path.append("/home/karisu/spark/spark-1.3.0/python")

sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'python') )
sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'bin') )
sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'python/lib/py4j-0.8.2.1-src.zip') )

try:
    from pyspark import SparkContext
    from pyspark import RDD
    from pyspark import SparkConf
    import pyspark

    print ("Successfully imported Spark Modules")

except ImportError as e:
    print ("Can not import Spark Modules", e)
    sys.exit(1)

class JoinExample(object):


    def main(self):
        conf=SparkConf()
        ## use XX gigabytes of RAM
        conf.set("spark.executor.memory", "22g")
        ## use up to 10 cores
        conf.set("spark.cores.max", "10")
        ## set the name of the application
        conf.setAppName("join")

        ## Initialize SparkContext
        sc = SparkContext('local[8]', conf=conf)

        ## load the first file
        file_1 = sc.textFile("/home/karisu/development/etl/sm_talk/input/join_A.txt")

        ## load the second file
        file_2 = sc.textFile("/home/karisu/development/etl/sm_talk/input/join_B.txt")

        # compute the url id from the url and decode the metrics
        rdd_1 = file_1.map(lambda line: (
            hashlib.md5(ujson.decode(line)[u'url']).hexdigest(),
            ujson.decode(line)['visits']))

        # compute the url id and get the metrics
        rdd_2 = file_2.map(lambda line: (
            hashlib.md5(ujson.decode(line)[u'url']).hexdigest(),
            ujson.decode(line)['conversion_rate']))

        assert isinstance(rdd_1, pyspark.rdd.RDD)

        # join both sets and sort them by conversion rate
        joined_rdd = rdd_1.\
            join(rdd_2).\
            map(lambda (key, value): (key, value[0], int(value[1]))).\
            sortBy(lambda x: 1*x[2])

        return joined_rdd



if __name__ == "__main__":
    obj_test = JoinExample()
    rdd = obj_test.main()


    print rdd.collect()



    sleep(2)
    print "done"