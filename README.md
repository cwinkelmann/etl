# README #
## Techtalk - MapReduce:  Hadoop && Spark Hands On ##

## What is MapReduce? ##
Algorithm proposed by Google to parallize Computations

## What is Hadoop? ##
simply a Implementation of MapReduce and a framework for reliable, scalable, distributed computing. 

## What is Spark? ##
Spark offers over 80 high-level operators that make it easy to build parallel apps. And you can use it interactively from the Scala, Python and R shells.

Run programs up to 100x faster than Hadoop MapReduce in memory, or 10x faster on disk.
Spark has an advanced DAG execution engine that supports cyclic data flow and in-memory computing.
[spark](https://spark.apache.org)

## What does Map and Reduce mean ? ##  

### Map ###
Mapper maps input key/value pairs to a set of intermediate key/value pairs.
basically this is the same as in other programming languages
[php](http://php.net/manual/de/function.array-map.php)
[javascript jquery ](http://api.jquery.com/jquery.map/)

example input:
```
#!javascript
["hello", "world", "hello"]
```

iterate over every element and create a tuple

example output ( use the word as a key, use "1" as a value)

```
#!javascript
[("hello",1), ("world", 1), ("hello",1)]
```

### Reducer ###
Reducer reduces a set of intermediate values which share a key to a smaller set of values.

input all map results sharing the same key

#### reducer 1 ####
```
#!javascript
[("hello",1), ("hello",1)]
```

#### reducer 2 ####

```
#!javascript
[("world", 1)]
```
possible output when just summing up the values

```
#!javascript
[("hello",2), ("world",1)]
```

## Examples: ##

### wordcount ### 
see 1_word_count.py

Count the the words in the following text:

"this is a hello world
example to show the world 
how the hello world example looks in hadoop
and why it should be called world count instead of 
word count"


### join ###
see 2_join.py
Join two Datasets using a single key

A:
```
#!javascript
{"url": "www.searchmetrics.com", "visits":"123"}
{"url": "www.searchmetrics.com/", "visits":"456"}
{"url": "blog.searchmetrics.com/", "visits":"456"}

```
B:
```
#!javascript

{"url": "www.searchmetrics.com", "conversion_rate":"8"}
{"url": "www.searchmetrics.com/", "conversion_rate":"4"}
{"url": "blog.searchmetrics.com/", "conversion_rate":"9"}
```


#### Desired Result: ####

```
#!javascript
[
	('32608a733d5bfa7e6186051fe2dfadbb', u'456', u'4'), 
	('60c2340275b49665776c9dd9fd6b2b40', u'456', u'9'), 
	('333f24da2bf3ce630d7e6eaa865c1706', u'123', u'8')
]

```

## setup a small cluster ##
./start-master.sh
./start-slave.sh

## big data example ##

create sample data
3_1_seo_vis.py

join the sample data
3_2_seo_vis.py



### spark streaming ###
run 4_streaming and start netcat 

## SQL context ##
[use sql in spark](https://github.com/apache/spark/blob/d874f8b546d8fae95bc92d8461b8189e51cb731b/examples/src/main/python/sql.py)


## What Spark can do for you ##

### Backend ###
write very small code snippets which can be executed on hadoop/hdfs or just the local storage.

### dataprovisioning ###
maybe more fun than cascading

### DataScience ###
use machine Learning Libraries like MLib [Latent Dirichlet Allocation](https://spark.apache.org/docs/latest/mllib-clustering.html#latent-dirichlet-allocation-lda), 
[Mahout](https://mahout.apache.org/users/sparkbindings/play-with-shell.html) 


### Business Intelligence ###
Use Tableau and Spark to explore raw data [tableau + spark]( http://www.tableau.com/de-de/about/blog/2014/10/tableau-spark-sql-big-data-just-got-even-more-supercharged-33799 )


## little homework and benchmark ##

### Implement a basic collaborative filtering algorithm in map reduce. ###
[similarity computation in map reduce](http://wanlab.poly.edu/recsys12/recsys/p163.pdf)

It would be great to see the results in R, cascading, plain vanilla Hadoop and spark.