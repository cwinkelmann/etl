__author__ = 'christian'
import ujson
import os
import sys


# Path for spark source folder
os.environ['SPARK_HOME']="/home/karisu/spark/spark-1.4.0"
# Append pyspark  to Python Path

sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'python') )
sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'bin') )
sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'python/lib/py4j-0.8.2.1-src.zip') )

try:
    from pyspark import SparkContext
    from pyspark import RDD
    from pyspark import SparkConf
    import pyspark
    from pyspark import SparkContext
    from pyspark.streaming import StreamingContext

    print ("Successfully imported Spark Modules")

except ImportError as e:
    print ("Can not import Spark Modules", e)
    sys.exit(1)

class StreamingExample(object):


    def main(self):
        conf=SparkConf()
        ## use XX gigabytes of RAM
        conf.set("spark.executor.memory", "20g")
        ## use up to 10 core
        conf.set("spark.cores.max", "8")
        ## set the name of the application
        conf.setAppName("streaming example")

        # Create a local StreamingContext with two working threads and
        # batch interval of 16 seconds
        sc = SparkContext("local[2]", "NetworkWordCount", conf=conf)
        ssc = StreamingContext(sc, 15)


        # Create a DStream that will connect to hostname:port, like localhost:9999
        lines = ssc.socketTextStream("localhost", 9999)

        # Split each line into words
        words = lines.flatMap(lambda line: line.split(" "))


        # Count each word in each batch
        pairs = words.map(lambda word: (word, 1))
        wordCounts = pairs.reduceByKey(lambda x, y: x + y)

        # Print the first ten elements of each RDD generated in this DStream to the console
        wordCounts.pprint()

        ssc.start()             # Start the computation
        ssc.awaitTermination()  # Wait for the computation to terminate



if __name__ == "__main__":
    """ run this in a terminal: nc.traditional -lp 9999 """
    obj_test = StreamingExample()
    rdd = obj_test.main()

