from time import sleep
from etl.model.datagenerator.Basic import Basic

__author__ = 'christian'
import os
import sys

# Path for spark source folder
os.environ['SPARK_HOME']="/home/karisu/spark/spark-1.4.0"
# Append pyspark  to Python Path

sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'python') )
sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'bin') )
sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'python/lib/py4j-0.8.2.1-src.zip') )

try:
    from pyspark import SparkContext, StorageLevel
    from pyspark import SparkConf

    print ("Successfully imported Spark Modules")

except ImportError as e:
    print ("Can not import Spark Modules", e)
    sys.exit(1)

class SparkTest(object):
    """
    create a huge set of sample data
    """

    def main(self, num_rankings, num_keywords):
        """

        :param num_rankings:
        :param num_keywords:
        :return: rdd
        """

        conf=SparkConf()
        conf.set("spark.executor.memory", "22g")
        conf.set("spark.cores.max", "10")
        conf.setAppName("sm_demo_1")
        conf.set("spark.files.overwrite", "true")




        ## Initialize SparkContext
        sc = SparkContext('local[6]', conf=conf)
        obj_BasicDatagenerator = Basic()

        # just create a set of numbers from 1 to num_rankings
        rdd_rankings = sc.parallelize(xrange(num_rankings), 8)
        rdd_rankings = obj_BasicDatagenerator.generate(rdd_rankings)
        rdd_rankings = rdd_rankings.cache() ## put the data into memory for later use

        # create the sample keywords
        rdd_keywords = sc.parallelize(xrange(num_keywords), 8)
        rdd_keywords = obj_BasicDatagenerator.generateKeywords(rdd_keywords)
        rdd_keywords = rdd_keywords.cache() ## put the data into memory for later use


        #rdd_rankings.saveAsTextFile("/home/karisu/development/etl/sm_talk/output/20150619/rankings/")
        #rdd_keywords.saveAsTextFile("/home/karisu/development/etl/sm_talk/output/20150619/keywords/")

        return rdd_rankings, rdd_keywords

if __name__ == "__main__":
    obj_test = SparkTest()
    num_rankings = 1000000 # hundred million rankings
    num_keywords = 6000000 # sixty million keywords

    rdd_rankings, rdd_keywords = obj_test.main(num_rankings, num_keywords)

    print rdd_rankings.count()

    print rdd_rankings.take(10)


    print rdd_keywords.count()
    print rdd_keywords.take(10)



    sleep(2)
    print "done"