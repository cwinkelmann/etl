
__author__ = 'christian'
import ujson
import os
import sys


# Path for spark source folder
os.environ['SPARK_HOME']="/home/karisu/spark/spark-1.4.0"
# Append pyspark  to Python Path

sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'python') )
sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'bin') )
sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'python/lib/py4j-0.8.2.1-src.zip') )

try:
    from pyspark import SparkContext
    from pyspark import RDD
    from pyspark import SparkConf
    import pyspark

    print ("Successfully imported Spark Modules")

except ImportError as e:
    print ("Can not import Spark Modules", e)
    sys.exit(1)

class JoinExample2(object):
    """
    demonstrate how to join a huge set of data
    """


    def main(self):
        conf=SparkConf()
        ## use XX gigabytes of RAM
        conf.set("spark.executor.memory", "20g")
        ## use up to X cores
        conf.set("spark.cores.max", "7")
        ## overwrite output files
        conf.set("spark.files.overwrite", "true")
        ## set the name of the application
        conf.setAppName("join rankings")

        ## Initialize SparkContext
        ## connect to the master
        #sc = SparkContext('spark://Ubuntu-1404-trusty-64-minimal:7077', conf=conf)
        ## local spark context
        sc = SparkContext('local', conf=conf)

        rankings = sc.textFile("/home/karisu/development/etl/sm_talk/output/20150619/keywords/part-*")

        keywords = sc.textFile("/home/karisu/development/etl/sm_talk/output/20150619/rankings/part-*")


        # get the keyword data
        rdd_rankings = rankings.sample(False, 1)\
            .map(lambda line: (ujson.decode(line)['kwid'], ujson.decode(line)))\
            .cache()

        # get the rankings data
        rdd_keywords = keywords.sample(False, 1)\
            .map(lambda line: (ujson.decode(line)['kwid'], ujson.decode(line))).\
            cache()

        assert isinstance(rdd_rankings, pyspark.RDD)

        # join both sets
        joined_rdd = rdd_keywords.join(rdd_rankings)
        joined_mapped_rdd = joined_rdd.map(lambda (key, value):
                    (value[1]['kwid'], value[1]['sv'], value[1]['cl'], value[0]['url_id'], value[0]['pos']))
        assert isinstance(joined_mapped_rdd, pyspark.RDD)
        return joined_mapped_rdd.coalesce(7)



if __name__ == "__main__":
    obj_test = JoinExample2()
    rdd = obj_test.main()

    rdd = rdd.cache()

    print rdd.take(100)

    rdd.saveAsTextFile("/home/karisu/development/etl/sm_talk/output/joined_rankings/20150619")


