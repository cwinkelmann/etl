from time import sleep
import os
import sys


# Path for spark source folder
os.environ['SPARK_HOME']="/home/karisu/spark/spark-1.4.0"
# Append pyspark  to Python Path

sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'python') )
sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'bin') )
sys.path.append( os.path.join(os.environ['SPARK_HOME'], 'python/lib/py4j-0.8.2.1-src.zip') )

try:
    from pyspark import SparkContext, StorageLevel
    from pyspark import SparkConf

    print ("Successfully imported Spark Modules")

except ImportError as e:
    print ("Can not import Spark Modules", e)
    sys.exit(1)

class WordCountExample(object):
    """

    """

    def main(self):
        """

        """
        conf=SparkConf()
        ## use XX gigabytes of RAM
        conf.set("spark.executor.memory", "22g")
        ## use up to 10 core
        conf.set("spark.cores.max", "10")
        ## set the name of the application
        conf.setAppName("word_count_1")

        ## Initialize SparkContext
        sc = SparkContext('local[8]', conf=conf)

        text_file = sc.textFile("/home/karisu/development/etl/sm_talk/input/word_count.txt")
        """
        split the words by a single whitespace
        map into key value pairs
        reduce to get the sums
        """
        text_file.persist(StorageLevel(False, True, False, True))
        counts = text_file.flatMap(lambda line: line.split(" ")).\
            map(lambda word: (word, 1)).\
            reduceByKey(lambda a, b: a + b). \
            sortBy(lambda x: -1*x[1])


        return counts


if __name__ == "__main__":
    obj_test = WordCountExample()
    rdd = obj_test.main()


    print rdd.collect()

    sleep(2)
    print "done"
