import random
import ujson

__author__ = 'christian'


class Basic(object):
    """
    allow the generation of rankings Data an develop it that way so it can be put into spark
    """


    def generate(self, rdd):
        """
        :param sc: SparkContext

        :return:
        """

        rdd = self.rankingsGenerator(rdd)


        return rdd


    def rankingsGenerator(self, rdd):
        """

        :param rdd:
        :return:
        """

        # return rdd.map(lambda x: ("a" * x ))
        # return rdd.map(lambda x: ( ("a" * x, "b" * x ) ))
        return rdd.map(lambda x: (ujson.dumps({'kwid': x,
                                                "pos": random.randint(1, 50),
                                               "url_id": random.randint(1000, 90000)})))


    def generateKeywords(self, rdd_keywords):
        """


        :rtype : RDD
        :param rdd_keywords:
        :return:
        """
        return rdd_keywords.map(lambda x: (ujson.dumps({'kwid': x,
                                               "cl": random.randint(1, 5),
                                                        "sv": random.randint(1, 100)})))

